def read_matrix(file):
    f = []
    for i in xrange(int(file.readline())):
        f.append(map(int, file.readline().split(' ')))
    return f

def compare(m, t):
    for i in xrange(len(m)):
        if m[i] < t[i]:
            return False
    return True

def bilding_of_tree(f_p, f_t, list_of_labels, dictionary, m, word, branch, k):

    if k == 6:
        return

    fl = 0
    for i in xrange(len(f_t)):
        t = []
        for j in xrange(len(f_p)):
            t.append(f_p[j][i])

        if compare(m, t):
            print
            fl = 1
            next_m = [0] * len(m)
            for j in xrange(len(f_p)):
                next_m[j] = m[j] - f_p[j][i] + f_t[i][j]

            next_word = word + 't' + str(i + 1)
            dictionary.append(next_word)

            print '\t'*k, 't' + str(i + 1) + ':', next_m,
            if next_m in list_of_labels:       
                print 'Iteration M' + str(list_of_labels.index(next_m)),
                if list_of_labels.index(next_m) in branch:
                    print 'Cycle',
                continue 
    
            print 'M' + str(len(list_of_labels)),

            list_of_labels.append(next_m)
    
            next_branch = branch[:]
            next_branch.append(len(list_of_labels) - 1)
    
            bilding_of_tree(f_p, f_t, list_of_labels, dictionary, next_m, next_word, next_branch, k + 1)
            
    if fl == 0:
        print 'Deadlock',
    return 

if __name__ == "__main__":
    file = open("input_lab1.txt", "r")

    f_p = read_matrix(file)
    f_t = read_matrix(file)
    m = map(int, file.readline().split(' '))

    print 'Tree of labels', '\n', 'M0:', m, 'M0',
    list_of_labels = [m]
    dictionary = ['m']
    bilding_of_tree(f_p, f_t, list_of_labels, dictionary, m, '', [0], 1)
    dictionary.sort(key = len)
    print '\n\nDictionary of context-free language\n', dictionary
